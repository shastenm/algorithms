1.  \# Write a Python program to construct the following pattern, using a nested loop number.
2.  \# 1
3.  \# 22
4.  \# 333
5.  \# 4444
6.  \# 55555
7.  \# 666666
8.  \# 7777777
9.  \# 88888888
10.  \# 999999999

\`\`\`
for i in range(10):
  print(str(i) \* i)
 \`\`\`